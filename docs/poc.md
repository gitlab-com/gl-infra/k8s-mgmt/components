### Overview of the PoC

The current POC was designed to prove the following concepts:

* Fully unattended GitLab instance installation and reconciliation using GitLab official GitOps system - FluxCD
* Ability to dynamically shift the inbound traffic using Istio Service Mesh technology

### Reasons for proving these two concepts

#### Unattended GitLab instance installation

Due to the company strategy related to improving the scalability of our SaaS platform and introducing a concept of Cells, we would like to simplify the management of our GitLab instances and treat them as cattle instead of pets. In order to achieve this we need to install or reconfigure instances in a completely automated manner. No manual interventions or steps are allowed during this process.
We also need to build significantly more instances in the future, and the manual or semi-automated installation process doesn't scale when we have dozens or hundreds of instances spread across the globe.
GitOps approach helps us to achieve that. Here are the advantages GitOps approach brings:

* Since Git is the single source of truth, all changes and updates are tracked and made visible. This ensures that the state of the infrastructure is always known and can be easily rolled back to any state if necessary.
* GitOps ensures that all environments, from development to production, are kept in sync, eliminating the possibility of configuration drift.
* GitOps allows for continuous delivery of infrastructure changes, enabling us to deploy changes quickly and easily.
* GitOps is highly scalable, making it easy to manage large, complex infrastructure deployments.


#### Traffic shifting using Service Mesh

Having the ability to dynamically shift the traffic between multiple GitLab instances is an essential part of enhanced deployment strategies such as Canary, Blue/Green, or A/B testing. In modern Cloud Native environments, this can be done using Service Mesh technologies. Service Mesh provides a set of Kubernetes CRD (Custom Resource Definition) resources that define the service endpoints, routing rules, and network policies. We can manually manipulate the traffic by adjusting these resources or use some progressive rollout frameworks like [Flagger](https://flagger.app/) to do this automatically, based on certain conditions.
We have chosen Istio and Flagger tools for traffic routing, as it's more mature and stable solution currently on the market.

### Lessons learned and possible next steps

#### Results

* We have proved that GitOps approach works very well for instantiating GitLab instance on newly created k8s cluster
* We achieved non-attended installation of GitLab instance and all required components to expose it to global loadbalancer. Including the automatic creation of cluster endpoints, issuing Let's Encrypt certificates and routing the traffic to GitLab instance services. This all now can be done with only few lines of Terraform code, which instantiates FluxCD system.
* We configured Istio Service Mesh to accept and route the traffic to GitLab services
* We configured Istio Service Endpoints to allow GitLab application components to access external out of mesh services
* We partially migrated an HaProxy middleware configuration to Istio Virtual services, which will help us to eliminate HAProxy layer in the future
* We tested manual procedures to shift the traffic to the different application versions, by assigning the different endpoint weights
* We tested Image Automation framework and defined an automatic procedure to rollout the new application versions once the image with a specific tag hits the OCI repository.
* We tested the basic progressive rollout automation using Flagger with a combination of Flux Image Automation framework.
* We tested failure scenarios where the new version didn't pass the smoke tests.

#### Next steps

* In order to continue with this Epic we would have to build a new dedicated environment for testing. Current 'PRE' clusters are interfering with each other and as we are using PRE cluster for release management, this blocks us from any further tests.
* We need to complete HAProxy layer migration to Istio components
* We need to investigate all possible options for global loadbalancer
* We only developed very basic progressive rollout scenario with fake tests. We need to integrate QA testing framework and pipelines with Flagger configuration
* We need to adjust chatops system to work with Flux/Istio/Flagger
* We need to invent an effort to create a baseline monitoring

### Links to relevant repos and READMEs for components that will remain in use:

All documentation about different POC components can be found in [Flux GitLab components repository](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components/-/tree/main/gitlab)