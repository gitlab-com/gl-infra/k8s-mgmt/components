# Shared Flux Components

This repository contains a set of shared Flux/GitOps components to be used in individual GitOps setups under the [Gitlab Tenants group](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants).

## What is GitOps?

From the GitOps topic page on Gitlab: "GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation."

It consists of three core components:
- Git repository as single source of truth
- MRs to make changes
- Automated infrastructure for CI/CD

Read more on: https://about.gitlab.com/topics/gitops/

## What is Flux?

Flux is a continuous delivery tool for keeping our Kubernetes clusters in sync with our git repositories.

Read more on the official Flux documentation: https://fluxcd.io/flux/

## What is Kustomize/`kustomization.yaml`?

`Kustomize` is a tool to customize Kubernetes objects through a `kustomization.yaml` file. In this repository, it's used to reference the other resource (`.yaml`) files declared in that folder, so we can reference it using `Kustomization` resource in a tenant repository. (Refer to the example provided in "Where are these components used?" section)

Read more about `kustomization` files: https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/

## Where are these components used?

The following repos form the GitOps workflow:

- [`clusters`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters) bootstraps Flux, sources the `base` component and configures `tenants` for each cluster
- [`tenants`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants) source the `components`, customize, and install them per cluster
- [`components`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components) this repo, re-usable and generic service "packages", ready to be customized and installed per env/cluster

Each folder/component in this repository (`components`) is a Kustomize folder, referenced by a Flux `Kustomization` resource in the different tenant repositories under the [Gitlab Tenants group](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants). For example, the resources defined under [the gitlab folder in this repository](./gitlab) is referenced by the [`Kustomization` resource named `flux-component-gitlab` in `delivery` tenant group](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/delivery/-/blob/main/components/gitlab/flux-components-gitlab.yaml), where it can be customized to that group's deployment.

The git repositories (`GitRepository` resources) are stored per cluster and tenant level under the [Clusters](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters/-/tree/main/) repository. For example, the `GitRepository` resource for this repository for `pre-gl-gke-2` cluster is deployed from https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters/-/blob/main/gke/gitlab-pre/us-east1/pre-gl-gke-2/components-base.yaml

## Component standards

One service/component per folder, isolated to a single namespace.

Components should be re-usable and shouldn't define any environment or cluster specific defaults, they can make some assumptions in defaults according to GitLab infrastructure needs.

Environment values (Helm) are controlled by a [`tenant`](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants), using configMaps, [cluster level variables](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters/-/blob/main/gke/gitlab-pre/us-east1/pre-gitlab-gke/cluster-vars.yaml) are used for templating.
Kustomization overlays are also an option to change/override manifest attributes, however avoid them if possible as they increase complexity and reduce visibility.

The `base` component is used and sourced directly by all clusters (bypassing tenants), it should contain the absolute minimum resources needed to bootstrap a cluster.

Helm is the prefered templating language and should be used as a first option.

Helm values overlays should be delegated to tenants with the following format:

```
<tenant>-overlay-<component-name>-values-${cluster}
```

## Locally testing/deploying changes (only in experimental environments)

Testing changes and diff can be done using the [Flux CLI](https://fluxcd.io/flux/cmd/). This may differ for different group's tenants, so refer to documentation on the repositories in the [tenants group](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants).

General steps to locally generate diffs or deploy changes made in this repository are:
1. Install [Flux CLI](https://fluxcd.io/flux/cmd/)
1. Push changes made in this repository to a branch
1. Locally connect to the cluster you wish to test on or deploy to via `glsh` or `gcloud` CLI. Refer to the documentation on how to [set up Kubernetes API Access](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#kubernetes-api-access).
1. Manually modify the `GitRepository` resource on the cluster to point to that branch (temporarily)
1. Locally navigate to [tenants group](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants) repository
1. Run `flux diff kustomization` or `flux create kustomization` on the component folder (e.g. `flux diff kustomization istio ./components/istio` inside of [tenants/delivery repository](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/delivery/-/tree/main/))
1. Change back the `GitRepository` resource if Flux to point to `main` branch
