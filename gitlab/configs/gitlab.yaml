apiVersion: v1
kind: ConfigMap
metadata:
  name: flux-components-gitlab-config-gitlab
  namespace: gitlab
data:
  config-gitlab: |
    gitlab:
        gitlab-exporter:
            enabled: false
        gitlab-pages:
            common:
                labels:
                    deployment: web-pages
                    type: web-pages
            deployment:
                strategy:
                    rollingUpdate:
                        maxSurge: 25%
                        maxUnavailable: 0
            extraEnv:
                FF_ENABLE_PLACEHOLDERS: "true"
                FF_ENABLE_REDIRECTS: "true"
                FF_ENFORCE_DOMAIN_RATE_LIMITS: "true"
                FF_ENFORCE_DOMAIN_TLS_RATE_LIMITS: "true"
                FF_ENFORCE_IP_RATE_LIMITS: "true"
                FF_ENFORCE_IP_TLS_RATE_LIMITS: "true"
                FF_HANDLE_CACHE_HEADERS: "true"
                GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-pages
            headers:
                - 'Permissions-Policy: interest-cohort=()'
            hpa:
                cpu:
                    targetAverageValue: 400m
                minReplicas: 1
            internalGitlabServer: http://gitlab-webservice-api.gitlab.svc:8181
            maxUnavailable: 5%
            networkpolicy:
                egress:
                    enabled: true
                    rules:
                        - ports:
                            - port: 8181
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 53
                              protocol: UDP
                            - port: 53
                              protocol: TCP
                        - ports:
                            - port: 988
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.252/32
                        - ports:
                            - port: 80
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.254/32
                        - to:
                            - ipBlock:
                                cidr: 0.0.0.0/0
                                except:
                                    - 10.0.0.0/8
                                    - 169.254.169.252/32
                                    - 169.254.169.254/32
                enabled: false
            propagateCorrelationId: true
            rateLimitDomain: 100
            rateLimitDomainBurst: 500
            rateLimitSourceIP: 20
            rateLimitSourceIPBurst: 300
            rateLimitTLSDomain: 30
            rateLimitTLSDomainBurst: 100
            rateLimitTLSSourceIP: 20
            rateLimitTLSSourceIPBurst: 50
            resources:
                limits:
                    memory: 1G
                requests:
                    cpu: 500m
                    memory: 70M
            sentry:
                dsn: https://11221aa4bbdf40f3ad857b8959012b23@sentry.gitlab.net/104
                enabled: true
                environment: ${environment}
            service:
                type: ClusterIP
                customDomains:
                    type: ClusterIP
            serviceAccount:
                annotations:
                    iam.gke.io/gcp-service-account: gitlab-gitlab-pages@${project}.iam.gserviceaccount.com
            useHTTPProxy: true
            useProxyV2: true
            zipCache:
                expiration: 300s
        gitlab-shell:
            common:
                labels:
                    deployment: gitlab-shell
                    type: git
            config:
                clientAliveInterval: 30
                maxStartups:
                    full: 400
                    rate: 60
                    start: 200
            deployment:
                strategy:
                    rollingUpdate:
                        maxSurge: 25%
                        maxUnavailable: 0
            enabled: true
            extraEnv:
                GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
                USE_GITLAB_LOGGER: 1
            global:
                shell:
                    port: 2222
            hpa:
                cpu:
                    targetAverageValue: 800m
            logging:
                format: json
            maxReplicas: 5
            maxUnavailable: 5%
            metrics:
                enabled: true
            minReplicas: 2
            resources:
                limits:
                    memory: 1G
                requests:
                    cpu: 1
                    memory: 1G
            service:
                type: ClusterIP
            serviceAccount:
                annotations:
                    iam.gke.io/gcp-service-account: gitlab-gitlab-shell@${project}.iam.gserviceaccount.com
            sshDaemon: gitlab-sshd
            workhorse:
                serviceName: webservice-internal-api
        kas:
            common:
                labels:
                    deployment: kas
                    type: kas
            customConfig:
                agent:
                    listen:
                        connections_per_token_per_minute: 20000
                gitaly:
                    global_api_rate_limit:
                        bucket_size: 70
                        refill_rate_per_second: 30
                    per_server_api_rate_limit:
                        bucket_size: 40
                        refill_rate_per_second: 15
                redis:
                    read_timeout: 5s
                    write_timeout: 5s
            deployment:
                minReadySeconds: 60
                strategy:
                    rollingUpdate:
                        maxSurge: 100%
                        maxUnavailable: 0
                terminationGracePeriodSeconds: 3600
            maxReplicas: 5
            maxUnavailable: 5%
            minReplicas: 2
            networkpolicy:
                egress:
                    enabled: true
                    rules:
                        - ports:
                            - port: 53
                              protocol: UDP
                            - port: 53
                              protocol: TCP
                        - ports:
                            - port: 8181
                              protocol: TCP
                          to:
                            - podSelector:
                                matchLabels:
                                    gitlab.com/webservice-name: api
                        - ports:
                            - port: 9999
                              protocol: TCP
                            - port: 9998
                              protocol: TCP
                            - port: 2305
                              protocol: TCP
                        - ports:
                            - port: 6379
                              protocol: TCP
                        - ports:
                            - port: 443
                              protocol: TCP
                        - ports:
                            - port: 988
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.252/32
                        - ports:
                            - port: 80
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.254/32
                        - ports:
                            - port: 8155
                              protocol: TCP
                          to:
                            - podSelector:
                                matchLabels:
                                    app: kas
                enabled: false
            privateApi:
                secret: gitlab-kas-private-api-credential-v2
            resources:
                limits:
                    cpu: 500m
                    memory: 256M
                requests:
                    cpu: 100m
                    memory: 100M
            service:
                type: ClusterIP
            serviceAccount:
                annotations:
                    iam.gke.io/gcp-service-account: gitlab-kas@${project}.iam.gserviceaccount.com
            workhorse:
                host: gitlab-webservice-api.gitlab.svc
        mailroom:
            common:
                labels:
                    deployment: mailroom
                    type: mailroom
            enabled: true
            networkpolicy:
                egress:
                    enabled: true
                    rules:
                        - ports:
                            - port: 53
                              protocol: UDP
                            - port: 53
                              protocol: TCP
                        - ports:
                            - port: 988
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.252/32
                        - ports:
                            - port: 80
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.254/32
                        - ports:
                            - port: 993
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 0.0.0.0/0
                                except:
                                    - 10.0.0.0/8
                                    - 169.254.169.252/32
                                    - 169.254.169.254/32
                        - ports:
                            - port: 6379
                              protocol: TCP
                            - port: 26379
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 443
                              protocol: TCP
                            - port: 11443
                              protocol: TCP
                            - port: 8181
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                enabled: false
            workhorse:
                serviceName: webservice-api
        migrations:
            enabled: false
        sidekiq:
            common:
                labels:
                    type: sidekiq
            enabled: true
            extraEnv:
                BYPASS_SCHEMA_VERSION: "true"
                ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                GITLAB_MEMORY_WATCHDOG_ENABLED: "true"
                GITLAB_RAILS_CACHE_DEFAULT_TTL_SECONDS: "28800"
                SIDEKIQ_LOG_ARGUMENTS: "1"
                STACKPROF_ENABLED: "true"
                UNSTRUCTURED_RAILS_LOG: "false"
                USE_GITLAB_LOGGER: 1
                prometheus_rust_multiprocess_metrics: "true"
            extraEnvFrom:
                SUBSCRIPTION_PORTAL_ADMIN_EMAIL:
                    secretKeyRef:
                        key: email
                        name: gitlab-subscription-portal-admin-v1
                SUBSCRIPTION_PORTAL_ADMIN_TOKEN:
                    secretKeyRef:
                        key: token
                        name: gitlab-subscription-portal-admin-v1
                SUGGESTED_REVIEWERS_SECRET:
                    secretKeyRef:
                        key: shared_secret
                        name: gitlab-suggested-reviewers-secret-v1
            health_checks:
                port: 8092
            hpa:
                cpu:
                    targetAverageValue: 450m
            logging:
                format: json
            maxUnavailable: 5%
            memoryKiller:
                maxRss: 0
            metrics:
                log_enabled: true
                port: 8083
            networkpolicy:
                egress:
                    enabled: false
                    rules:
                        - to:
                            - ipBlock:
                                cidr: 0.0.0.0/0
                                except:
                                    - 10.0.0.0/8
                                    - 169.254.169.252/32
                                    - 169.254.169.254/32
                        - ports:
                            - port: 53
                              protocol: UDP
                            - port: 53
                              protocol: TCP
                        - ports:
                            - port: 988
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.252/32
                        - ports:
                            - port: 80
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.254/32
                        - ports:
                            - port: 8600
                              protocol: TCP
                            - port: 8600
                              protocol: UDP
                          to:
                            - namespaceSelector: {}
                              podSelector:
                                matchLabels:
                                    app: consul
                        - ports:
                            - port: 6379
                              protocol: TCP
                            - port: 26379
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 5432
                              protocol: TCP
                            - port: 6432
                              protocol: TCP
                            - port: 6433
                              protocol: TCP
                            - port: 6434
                              protocol: TCP
                            - port: 6435
                              protocol: TCP
                            - port: 6436
                              protocol: TCP
                            - port: 6437
                              protocol: TCP
                            - port: 6438
                              protocol: TCP
                            - port: 6439
                              protocol: TCP
                            - port: 6440
                              protocol: TCP
                            - port: 6441
                              protocol: TCP
                            - port: 6442
                              protocol: TCP
                            - port: 6443
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 9999
                              protocol: TCP
                            - port: 9998
                              protocol: TCP
                            - port: 2305
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 5000
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 6060
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                        - ports:
                            - port: 9090
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 10.0.0.0/8
                enabled: false
            pods:
                - common:
                    labels:
                        shard: catchall
                  concurrency: 25
                  maxReplicas: 5
                  minReplicas: 1
                  name: catchall
            psql:
                host: 10.33.1.14
            queueSelector: true
            registry:
                api:
                    host: registry.${global_endpoint}
                    port: 443
                    protocol: https
                tokenIssuer: omnibus-gitlab-issuer
            trusted_proxies:
                - 10.0.0.0/8
        toolbox:
            backups:
                objectStorage:
                    config:
                        key: empty
                        secret: empty
            enabled: false
        webservice:
            deployment:
                livenessProbe:
                    failureThreshold: 2
                    initialDelaySeconds: 0
                    periodSeconds: 30
                readinessProbe:
                    failureThreshold: 2
                    initialDelaySeconds: 0
                    periodSeconds: 2
                startupProbe:
                    failureThreshold: 60
                    httpGet:
                        path: /-/readiness
                        port: 8080
                        scheme: HTTP
                    initialDelaySeconds: 10
                    periodSeconds: 3
                    successThreshold: 1
                    timeoutSeconds: 2
                strategy:
                    rollingUpdate:
                        maxSurge: 20%
                        maxUnavailable: 5%
            deployments:
                api:
                    common:
                        labels:
                            type: api
                    deployment:
                        terminationGracePeriodSeconds: 65
                    extraEnv:
                        ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                        GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
                        GITLAB_METRICS_INITIALIZE: api
                        GITLAB_SENTRY_EXTRA_TAGS: '{"type": "api", "stage": "main"}'
                    ingress:
                        annotations:
                            nginx.ingress.kubernetes.io/proxy-buffer-size: 8k
                            nginx.ingress.kubernetes.io/proxy-buffering: "on"
                            nginx.ingress.kubernetes.io/proxy-buffers-number: "8"
                            nginx.ingress.kubernetes.io/proxy-request-buffering: "on"
                        enabled: true
                        path: /api
                    pod:
                        labels:
                            deployment: api
                    service:
                        type: ClusterIP
                    shutdown:
                        blackoutSeconds: 10
                    workerProcesses: 6
                    workhorse:
                        extraArgs: -apiLimit 9 -apiQueueDuration 30s -apiQueueLimit 2000 -apiCiLongPollingDuration 50s -propagateCorrelationID
                git:
                    common:
                        labels:
                            type: git
                    deployment:
                        terminationGracePeriodSeconds: 260
                    extraEnv:
                        ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                        GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
                        GITLAB_METRICS_INITIALIZE: git
                        GITLAB_SENTRY_EXTRA_TAGS: '{"type": "git", "stage": "main"}'
                    ingress:
                        path: null
                    pod:
                        labels:
                            deployment: git-https
                    service:
                        type: ClusterIP
                    shutdown:
                        blackoutSeconds: 0
                    workhorse:
                        keywatcher: false
                internal-api:
                    common:
                        labels:
                            type: internal-api
                    deployment:
                        terminationGracePeriodSeconds: 65
                    extraEnv:
                        ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                        GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-internal-api
                        GITLAB_METRICS_INITIALIZE: internal-api
                        GITLAB_SENTRY_EXTRA_TAGS: '{"type": "internal-api", "stage": "main"}'
                    hpa:
                        maxReplicas: 5
                        minReplicas: 2
                    ingress:
                        path: null
                    pod:
                        labels:
                            deployment: internal-api
                    resources:
                        requests:
                            cpu: 500m
                            memory: 1.25G
                    shutdown:
                        blackoutSeconds: 10
                    workerProcesses: 6
                    workhorse:
                        extraArgs: -apiLimit 9 -apiQueueDuration 30s -apiQueueLimit 2000 -apiCiLongPollingDuration 50s -propagateCorrelationID
                web:
                    common:
                        labels:
                            type: web
                    deployment:
                        terminationGracePeriodSeconds: 65
                    extraEnv:
                        ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                        GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-web
                        GITLAB_METRICS_INITIALIZE: web
                        GITLAB_SENTRY_EXTRA_TAGS: '{"type": "web", "stage": "main"}'
                        MALLOC_CONF: narenas:2
                    ingress:
                        annotations:
                            nginx.ingress.kubernetes.io/proxy-buffering: "on"
                            nginx.ingress.kubernetes.io/proxy-buffers-number: "8"
                            nginx.ingress.kubernetes.io/proxy-request-buffering: "on"
                        enabled: true
                        path: /
                    pod:
                        labels:
                            deployment: web
                    service:
                        type: ClusterIP
                    sharedTmpDir:
                        sizeLimit: 10G
                    shutdown:
                        blackoutSeconds: 10
                    sshHostKeys:
                        mount: true
                    workerProcesses: 4
                    workhorse:
                        keywatcher: false
                websockets:
                    common:
                        labels:
                            type: websockets
                    deployment:
                        readinessProbe:
                            initialDelaySeconds: 0
                        strategy:
                            rollingUpdate:
                                maxUnavailable: 0
                        terminationGracePeriodSeconds: 30
                    extraEnv:
                        ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
                        GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-websockets
                        GITLAB_METRICS_INITIALIZE: websockets
                        GITLAB_SENTRY_EXTRA_TAGS: '{"type": "websockets", "stage": "main"}'
                        GODEBUG: madvdontneed=1
                    ingress:
                        path: null
                    pod:
                        labels:
                            deployment: websockets
                    service:
                        type: ClusterIP
                    shutdown:
                        blackoutSeconds: 0
                    workhorse:
                        keywatcher: false
            enabled: true
            extraEnv:
                ACTION_CABLE_IN_APP: "true"
                BYPASS_SCHEMA_VERSION: "true"
                DISABLE_PUMA_NAKAYOSHI_FORK: "true"
                ENABLE_RBTRACE: 1
                GITLAB_MEMORY_WATCHDOG_ENABLED: "true"
                GITLAB_MEMWD_MAX_HEAP_FRAG: 0.3
                GITLAB_RAILS_CACHE_DEFAULT_TTL_SECONDS: "28800"
                STACKPROF_ENABLED: "true"
                UNSTRUCTURED_RAILS_LOG: "false"
                USE_GITLAB_LOGGER: 1
                prometheus_rust_multiprocess_metrics: "true"
            extraEnvFrom:
                ARKOSE_LABS_PRIVATE_KEY:
                    secretKeyRef:
                        key: private_key
                        name: gitlab-arkose-labs-keys-v1
                ARKOSE_LABS_PUBLIC_KEY:
                    secretKeyRef:
                        key: public_key
                        name: gitlab-arkose-labs-keys-v1
                GITLAB_GRAFANA_API_KEY:
                    secretKeyRef:
                        key: api_key
                        name: gitlab-grafana-api-key-v2
                SUBSCRIPTION_PORTAL_ADMIN_EMAIL:
                    secretKeyRef:
                        key: email
                        name: gitlab-subscription-portal-admin-v1
                SUBSCRIPTION_PORTAL_ADMIN_TOKEN:
                    secretKeyRef:
                        key: token
                        name: gitlab-subscription-portal-admin-v1
                SUGGESTED_REVIEWERS_SECRET:
                    secretKeyRef:
                        key: shared_secret
                        name: gitlab-suggested-reviewers-secret-v1
            extraInitContainers: |
                - name: write-instance-name
                  args:
                    - -c
                    - echo "$INSTANCE_NAME" > /etc/gitlab/instance_name
                  command:
                    - sh
                  env:
                    - name: INSTANCE_NAME
                      valueFrom:
                        fieldRef:
                          fieldPath: spec.nodeName
                  image: 'busybox:latest'
                  volumeMounts:
                    - mountPath: /etc/gitlab
                      name: webservice-secrets
            hpa:
                cpu:
                    targetAverageValue: 1600m
                maxReplicas: 5
                minReplicas: 2
            ingress:
                enabled: false
                proxyBodySize: 0
            maxReplicas: 10
            maxUnavailable: 5%
            minReplicas: 2
            monitoring:
                exporter:
                    enabled: true
                ipWhitelist:
                    - 10.0.0.0/8
                    - 127.0.0.0/8
                    - 169.254.169.252/24
            networkpolicy:
                egress:
                    enabled: false
                    rules:
                        - to:
                            - ipBlock:
                                cidr: 0.0.0.0/0
                                except:
                                    - 10.0.0.0/8
                                    - 169.254.169.252/32
                                    - 169.254.169.254/32
                        - ports:
                            - port: 53
                              protocol: UDP
                            - port: 53
                              protocol: TCP
                            - port: 8600
                              protocol: TCP
                            - port: 8600
                              protocol: UDP
                        - ports:
                            - port: 988
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.252/32
                        - ports:
                            - port: 80
                              protocol: TCP
                          to:
                            - ipBlock:
                                cidr: 169.254.169.254/32
                        - ports:
                            - port: 5000
                              protocol: TCP
                          to:
                            - podSelector:
                                matchLabels:
                                    app: registry
                        - ports:
                            - port: 8153
                              protocol: TCP
                          to:
                            - podSelector:
                                matchLabels:
                                    app: kas
                        - ports:
                            - port: 6379
                              protocol: TCP
                            - port: 26379
                              protocol: TCP
                        - ports:
                            - port: 5432
                              protocol: TCP
                            - port: 6432
                              protocol: TCP
                            - port: 6433
                              protocol: TCP
                            - port: 6434
                              protocol: TCP
                            - port: 6435
                              protocol: TCP
                            - port: 6436
                              protocol: TCP
                            - port: 6437
                              protocol: TCP
                            - port: 6438
                              protocol: TCP
                            - port: 6439
                              protocol: TCP
                            - port: 6440
                              protocol: TCP
                            - port: 6441
                              protocol: TCP
                            - port: 6442
                              protocol: TCP
                            - port: 6443
                              protocol: TCP
                        - ports:
                            - port: 9999
                              protocol: TCP
                            - port: 9998
                              protocol: TCP
                            - port: 2305
                              protocol: TCP
                        - ports:
                            - port: 9090
                              protocol: TCP
                        - ports:
                            - port: 6070
                              protocol: TCP
                          to:
                            - podSelector:
                                matchLabels:
                                    app.kubernetes.io/name: gitlab-zoekt
                enabled: false
            puma:
                disableWorkerKiller: true
                threads:
                    max: 4
                    min: 1
            rack_attack:
                git_basic_auth:
                    bantime: 3600
                    enabled: false
                    findtime: 180
                    maxretry: 30
            registry:
                tokenIssuer: omnibus-gitlab-issuer
            resources:
                limits:
                    memory: 4.0G
                requests:
                    cpu: 1
                    memory: 1.25G
            serviceAccount:
                annotations:
                    iam.gke.io/gcp-service-account: gitlab-webservice@${project}.iam.gserviceaccount.com
            serviceLabels:
                railsPromJob: gitlab-rails
                workhorsePromJob: gitlab-workhorse
            shutdown:
                blackoutSeconds: 0
            workerProcesses: 2
            workhorse:
                extraArgs: -apiLimit 5 -apiQueueDuration 60s -apiQueueLimit 200 -propagateCorrelationID
                imageScaler:
                    maxFileSizeBytes: 250000
                    maxProcs: 8
                livenessProbe:
                    initialDelaySeconds: 0
                monitoring:
                    exporter:
                        enabled: true
                readinessProbe:
                    initialDelaySeconds: 0
                resources:
                    limits:
                        memory: 1G
                    requests:
                        cpu: 100m
                        memory: 50M
                startupProbe:
                    exec:
                        command:
                            - /scripts/healthcheck
                    failureThreshold: 20
                    initialDelaySeconds: 5
                    periodSeconds: 2
                    successThreshold: 1
                    timeoutSeconds: 2
                trustedCIDRsForPropagation:
                    - 10.0.0.0/8
                    - 127.0.0.1/32
                trustedCIDRsForXForwardedFor:
                    - 10.0.0.0/8
                    - 127.0.0.1/32
