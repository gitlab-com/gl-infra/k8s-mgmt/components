apiVersion: v1
kind: ConfigMap
metadata:
  name: flux-components-gitlab-config-registry
  namespace: gitlab
data:
  config-registry: |
    registry:
        annotations:
            sidecar.istio.io/inject: "false"
        common:
            labels:
                deployment: registry
                type: registry
        database:
            enabled: true
            host: 10.33.1.2
            migrations:
                enabled: true
            name: registry_production
            password:
                secret: registry-postgresql-password-v2
            pool:
                maxidle: 5
                maxlifetime: 5m
                maxopen: 5
            user: registry
        debug:
            prometheus:
                enabled: true
        deployment:
            readinessProbe:
                initialDelaySeconds: 40
        draintimeout: 60s
        enabled: true
        extraEnv:
            SKIP_POST_DEPLOYMENT_MIGRATIONS: true
        gc:
            blobs:
                storagetimeout: 2s
            disabled: false
            maxbackoff: 30m
            noidlebackoff: true
            reviewafter: 5m
        health:
            storagedriver:
                enabled: true
        hpa:
            cpu:
                targetAverageUtilization: 70
        log:
            accesslog:
                formatter: json
            formatter: json
            level: debug
        maintenance:
            uploadpurging:
                enabled: false
        maxUnavailable: 5%
        middleware:
            storage:
                - name: googlecdn
                  options:
                    baseurl: cdn.registry.${environment}.gitlab-static.net
                    ipfilteredby: gcp
                    keyname: ${environment}-registry-cdn
                    privatekeySecret:
                        key: private-key
                        secret: registry-cdn-private-key-v2
        migration:
            disablemirrorfs: true
        networkpolicy:
            egress:
                enabled: false
                rules:
                    - ports:
                        - port: 5432
                          protocol: TCP
                        - port: 6379
                          protocol: TCP
                        - port: 6432
                          protocol: TCP
                        - port: 26379
                          protocol: TCP
                      to:
                        - ipBlock:
                            cidr: 10.0.0.0/8
                    - ports:
                        - port: 53
                          protocol: UDP
                        - port: 53
                          protocol: TCP
                    - ports:
                        - port: 988
                          protocol: TCP
                      to:
                        - ipBlock:
                            cidr: 169.254.169.252/32
                    - ports:
                        - port: 80
                          protocol: TCP
                      to:
                        - ipBlock:
                            cidr: 169.254.169.254/32
                    - to:
                        - ipBlock:
                            cidr: 0.0.0.0/0
                            except:
                                - 10.0.0.0/8
                                - 169.254.169.252/32
                                - 169.254.169.254/32
            enabled: false
        profiling:
            stackdriver:
                enabled: true
        redis:
            cache:
                dialtimeout: 2s
                enabled: true
                host: mymaster
                password:
                    enabled: true
                    key: redis-password
                    secret: redis-registry-cache-password-v1
                pool:
                    idletimeout: 5m
                    maxlifetime: 1h
                    size: 10
                readtimeout: 2s
                sentinels:
                    - host: redis-registry-cache-node-0.redis-registry-cache.${environment}.gke.gitlab.net
                      port: 26379
                    - host: redis-registry-cache-node-1.redis-registry-cache.${environment}.gke.gitlab.net
                      port: 26379
                    - host: redis-registry-cache-node-2.redis-registry-cache.${environment}.gke.gitlab.net
                      port: 26379
                writetimeout: 2s
        reporting:
            sentry:
                dsn: https://a40e1c5a81e0448094930f5680fe2e49@sentry.gitlab.net/122
                enabled: true
                environment: ${environment}
        resources:
            limits:
                memory: 3G
            requests:
                cpu: 300m
                memory: 1G
        service:
            type: ClusterIP
        serviceAccount:
            annotations:
                iam.gke.io/gcp-service-account: gitlab-registry@gitlab-${environment}.iam.gserviceaccount.com
            create: true
        storage:
            extraKey: gcs.json
            key: config
            secret: registry-storage-v6
        tokenIssuer: omnibus-gitlab-issuer
        validation:
            disabled: false
            manifests:
                payloadsizelimit: 256000
                referencelimit: 200
                urls:
                    allow:
                        - .*
