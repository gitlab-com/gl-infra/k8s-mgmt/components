## GitLab Flux component

This component installs and configures a GitLab instance on a given cluster. In order to include the GitLab component in the Flux configuration, the following Kustomization resource should be added.

```yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: flux-components-gitlab
  namespace: flux-system
spec:
  interval: 10m0s
  path: ./gitlab
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-components
  postBuild:
    substituteFrom:
      - kind: ConfigMap
        name: flux-cluster-vars
      - kind: ConfigMap
        name: flux-instance-vars
  healthChecks:
    - apiVersion: helm.toolkit.fluxcd.io/v2
      kind: HelmRelease
      name: gitlab
      namespace: gitlab
  dependsOn:
    - name: flux-components-base
```

**This component is environment agnostic, please don't include any environment related configuration or endpoints**

In order to configure this component to include environment or cluster specific value use variable substitutions.


### Variable substitutions

You can include the following variables in any resources.

#### Cluster wide variables

The following variables defined on the cluster level and might be different for each cluster. They define infrastructure topology and cluster type and position.

* `${provider}` - Cloud provider, ie. `gke`
* `${project}` - GCP project, AWS account or Azure subsctiption id
* `${region}` - Cloud region, ie. `us-west1`
* `${environment}` - application environment, ie. `pre`
* `${cluster}` - Kubernetes cluster name, ie. `pre-gl-gke-2`
* `${domain}` - Global DNS zone, which will be used to construct endpoints URL's, ie. `gitlab.com`

#### Instance wide variables

Usually defined in tenant overlays to configure GitLab instance itself. Variables like `database_address` can be added on demand.

* `${global_endpoint}` - Defines the global URL for the GitLab instance, ie. `pre.gitlab.com`

## Component resources

### Certificates

Let's Encrypt certificates issued for the instance endpoints. Each certificate uses `gitlab-combined-dns` cluster issuer to be able to include both: cluster specific and global domains. One from the Google DNS and another one from CloudFlare DNS services.

### Configs

Each file in this directory contains a `ConfigMap` resource for the specific GitLab helm chart values hash. For example the following configurations
```yaml
  config-nginx-ingress: |
    nginx-ingress:
        enabled: false
```

```yaml
  config-postgresql: |
    postgresql:
        install: false
```

will be merged together in the resulting `values.yaml` file which will be substituted to the `helm install -f` command by Flux helm controller.

```yaml
    nginx-ingress:
        enabled: false
    postgresql:
        install: false
```

#### Value precedence

Configs from these config maps will be overritten by `values:` hash from `gitlab.yaml` file in the root of this component.

```yaml
  values:
    registry:
      image:
        repository: us-east1-docker.pkg.dev/gitlab-com-artifact-registry/images/gitlab-container-registry # {"$imagepolicy": "gitlab:gitlab-container-registry:name"}
        tag: flux-pre-16-2-202307201900-ce615a975c9 # {"$imagepolicy": "gitlab:gitlab-container-registry:tag"}
```

### Image Automation

Set of resources used for enabling [Flux Image Automation](https://fluxcd.io/flux/components/image/) for GitLab instance. Image Automation updates Git repository when new container image is available.
Each subfolder corresponds to certain GitLab image, ie. `gitlab-webservice-ee` automates the updates on `us-east1-docker.pkg.dev/gitlab-com-artifact-registry/images/gitlab-workhorse-ee` container image.

There are three types of resources defined for Image Automation.

* `ImageRepository` - Managed by image-reflector-controller, scans the container repository and stores the list of the tags
* `ImagePolicy` - Filters the tags according to the regexp rule and sorts them. It stores the latest tag. In order to integrate GitLab coordinated pipeline with Flux, the image policy is set to search for the specific tags in our container registry which start with `flux-${environment}-` prefix followed by the usual `auto_deploy` version, ie. `flux-pre-16-2-202307201900-ce615a975c9`

Both `ImageRepository` and `ImagePolicy` are defined for each container image we use.

* `ImageUpdateAutomation` - Globally defined for the entire Gitlab component. Fetches the latest tags from `ImagePolicy` resources and updates components git repository according to the markers which are set in HelmRelease configuration. The `ImageUpdateAutomation` resource commits the changes using the @fluxcdbot Gitlab account.

#### Image Automation markers

All container image versions for the Gitlab component are defined as `values:` hash in `gitlab.yaml` file in the root folder of the component. Versions have a special markers which define how to find a version tag and what kind of `ImagePolicy` to use for the image update.

``` yaml
  values:
    registry:
      image:
        repository: us-east1-docker.pkg.dev/gitlab-com-artifact-registry/images/gitlab-container-registry # {"$imagepolicy": "gitlab:gitlab-container-registry:name"}
        tag: flux-pre-16-2-202307201900-ce615a975c9 # {"$imagepolicy": "gitlab:gitlab-container-registry:tag"}

```

The entire process of rolling out the versions looks like the following:

1. Coordinated pipeline builds an image and push the special tag to the container registry. Tags are different for each environment, but the image is the same.
2. `ImageRegistry` fetches the list of the tags from the container registry
3. `ImagePolicy` filters and sorts the tags fetched by `ImageRegistry`, it detects the latest tag for the specified image and environment
4. `ImageUpdateAutomation` compares the tags marked in `GitRepository` and latest images for defined `ImagePolicy`
5. `ImageUpdateAutomation` commits the updated version to the components git repository.
6. Flux Helm controller detects the change in `HelmRelease` resource and start reconciliation process.
7. Images updated according to Deployment rollout strategy or picked by Flagger `Canary` resouce.

#### Networking

Networking folder contains a set of resources to manage the traffic using Istio Service Mesh.

* `DestinationRule` - defines policies that apply to traffic intended for a service after routing has occurred. In our case we use or will use the number of `DestinationRule` resources to handle the traffic originated to outside of our GKE clusters for example, static assets stored on Google Storage buckets.
* `Gateway` - describes a public or internal load balancer operating at the edge of the mesh receiving incoming or outgoing HTTPS/TCP connections. `Gateway` defines the ports the load balancer listens on, hosts DNS names the load balancer responsible to, and other features like TLS configuration. More detailed information about Gateway resources can be found on [Istio documentation](https://istio.io/latest/docs/reference/config/networking/gateway/)
* `VirtualService` - defines the L7 routing rules applied to the service. This can be URI path-based routing, HTTP redirects, HTTP rewrite rules, etc. `VirtualService` has a reference to the `Gateway` load balancer resource and configures the routing for this specific load balancer. See [Istio VirtualService documentation](https://istio.io/latest/docs/reference/config/networking/virtual-service/) for more info.
* `ServiceEntry` - service entry describes the services which are external to the Service Mesh, but consumed by services running within the Service Mesh. Istio blocks all outbound traffic to any third-party resources like external databases or Redis clusters. In order to make these services available within Service Mesh, you need to specify a new ServiceEntry resource and define `endpoints` which will be added to the Service Mesh network from the outside.